package test.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import test.model.Customer;
import test.util.DataSourceProvider;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDao {

    public List<Customer> getCustomers() {

        List<Customer> customerList = new ArrayList<>();

        try (Connection conn = DataSourceProvider.getDataSource().getConnection();
            Statement stmt = conn.createStatement()) {

            try (ResultSet r = stmt.executeQuery("select * from customer")) {
                while (r.next()) {
                    customerList.add(new Customer(
                            r.getLong(1),
                            r.getString(2),
                            r.getString(3),
                            r.getString(4)
                    ));
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return customerList;
    }

    public void insertCustomer(Customer customer) {

        String query = "INSERT INTO customer (id, firstName, lastName, code) VALUES (NEXT VALUE FOR seq1, ?, ?, ?)";

        try (Connection conn = DataSourceProvider.getDataSource().getConnection();
             PreparedStatement ps = conn.prepareStatement(query)) {

            String firstName = customer.getFirstName() != null ? customer.getFirstName() : " ";
            String lastName = customer.getLastName() != null ? customer.getLastName() : " ";
            String code = customer.getCode() != null ? customer.getCode() : " ";

             ps.setString(1, firstName);
             ps.setString(2, lastName);
             ps.setString(3, code);

             ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public void deleteCustomers() {
        String query = "DELETE FROM customer";

        try (Connection conn = DataSourceProvider.getDataSource().getConnection();
             PreparedStatement ps = conn.prepareStatement(query)) {

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteCustomer(long id) {
        String query = "delete from customer where id = ?";

        try (Connection conn = DataSourceProvider.getDataSource().getConnection();
             PreparedStatement ps = conn.prepareStatement(query)) {

            ps.setLong(1, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Customer getCustomerForId(String customerId) {

        Customer customer = new Customer();

        try (Connection conn = DataSourceProvider.getDataSource().getConnection();

             PreparedStatement ps = conn.prepareStatement("SELECT id, firstName, lastName, code FROM customer WHERE id = ?")) {

            ps.setLong(1, Long.parseLong(customerId));

            System.out.println("index: " + customerId);

            try (ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    customer.setId(rset.getLong(1));
                    customer.setFirstName(rset.getString(2));
                    customer.setLastName(rset.getString(3));
                    customer.setCode(rset.getString(4));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return customer;
    }
}
