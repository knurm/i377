package test;

import com.fasterxml.jackson.databind.ObjectMapper;
import test.dao.CustomerDao;
import test.model.Customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet("/customers/form")
public class CustomersFormServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    CustomerDao customerDao = new CustomerDao();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String name = req.getParameter("name");

        Customer customer = new Customer();

        customer.setFirstName(name);

        customerDao.insertCustomer(customer);

    }

}
