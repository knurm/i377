package test;

import com.fasterxml.jackson.databind.ObjectMapper;
import test.dao.CustomerDao;
import test.model.Customer;
import test.util.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/api/customers")
public class CustomersServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    CustomerDao customerDao = new CustomerDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");

        response.setContentType("application/json");

        if (id != null) {
            Customer customer = customerDao.getCustomerForId(id);

            new ObjectMapper().writeValue(response.getOutputStream(), customer);
        } else {
            new ObjectMapper().writeValue(response.getOutputStream(), customerDao.getCustomers());
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = Util.asString(req.getInputStream());

        System.out.println(input);

        Customer customer = new ObjectMapper().readValue(input, Customer.class);

        customerDao.insertCustomer(customer);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id;

        System.out.println("id: " + req.getParameter("id"));

        if (req.getParameter("id") != null) {
            id = Long.parseLong(req.getParameter("id"));
            customerDao.deleteCustomer(id);
        } else {
            customerDao.deleteCustomers();
        }

    }

}
