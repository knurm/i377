package test;

import test.util.DataSourceProvider;
import test.util.DbUtil;
import test.util.FileUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@WebListener
public class StartupListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String url = "jdbc:hsqldb:mem:db";
        String schema = FileUtil.readFileFromClasspath("schema.sql");
        DataSourceProvider.setDbUrl(url);

        try (Connection c = DataSourceProvider.getDataSource().getConnection()) {
            DbUtil.insertFromFile(c, schema);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
